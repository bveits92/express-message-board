const express = require('express');
const router = express.Router();
const axios = require('axios');

// Function to fetch random user data from the API
async function fetchRandomUserData() {
  try {
    const response = await axios.get('https://randomuser.me/api/');
    const userData = response.data.results[0];
    return userData;
  } catch (error) {
    console.error('Error fetching random user data:', error);
    return null;
  }
}

// Define your messages array with random user data
const messages = [];

// Fetch random user data and populate messages array
(async () => {
  const userData = await fetchRandomUserData();
  if (userData) {
    const message = {
      text: 'Hello!',
      user: `Brad`,
      added: new Date(),
      img: userData.picture.thumbnail,
    };
    messages.push(message);
  }
})();

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express Message Board', messages: messages });
});

/* Get new message page. */
router.get('/new', function (req, res, next) {
  res.render('form', { title: 'Create a new message' });
});

/* Sending a post request for submitting the form */
router.post('/new', async function (req, res, next) {
  const name = req.body.name;
  const messageText = req.body.message;

  // Fetch random user data and get the thumbnail image URL
  const userData = await fetchRandomUserData();
  const img = userData.picture.thumbnail;

  // Create a new message object
  const message = {
    text: messageText,
    user: name,
    added: new Date(),
    img: img, // Set the img property to the thumbnail URL
  };

  // Push the message to the messages array
  messages.push(message);

  res.redirect('/');
});

module.exports = router;
